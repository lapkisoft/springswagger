package com.example.demo.controller;

import com.example.demo.model.RequestMessage;
import com.example.demo.model.ResponseMessage;
import com.example.demo.service.HelloService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
@RequiredArgsConstructor
@Api(description = "Hello контроллер")
public class HelloController {
    private final HelloService helloService;

    @PostMapping
    @ApiOperation("Генерирует приветствие по имени (настоящая магия)")
    public ResponseMessage hello(@RequestBody RequestMessage requestMessage) {
        return helloService.hello(requestMessage);
    }
}

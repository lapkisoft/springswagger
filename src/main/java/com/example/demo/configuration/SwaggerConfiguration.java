package com.example.demo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfiguration extends WebMvcConfigurationSupport {
    private final String apiPackage = "com.example.demo.controller";
    private final String apiPath = "/**";

    @Bean
    public Docket apiDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(apiPackage))
                .paths(PathSelectors.ant(apiPath))
                .build()
                .apiInfo(getApiInfo());
    }

    private final String title = "Demo API with Swagger";
    private final String description = "Lorem ipsum";
    private final String version = "v1";
    private final String url = "http://localhost:8080";
    private final String contactName = "Marina";
    private final String email = "test@test.ru";
    private final String license = "WTFPL";
    private final String licenseUrl = "https://ru.wikipedia.org/wiki/WTFPL";

    private ApiInfo getApiInfo() {
        return new ApiInfo(
                title,
                description,
                version,
                url,
                new Contact(contactName, url, email),
                license,
                licenseUrl,
                Collections.emptyList()
        );
    }

    private final String uiHtml = "swagger-ui.html";
    private final String uiHtmlLocation = "classpath:/META-INF/resources/";
    private final String webjars = "/webjars/**";
    private final String webjarsLocation = "classpath:/META-INF/resources/webjars/";

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(uiHtml)
                .addResourceLocations(uiHtmlLocation);
        registry.addResourceHandler(webjars)
                .addResourceLocations(webjarsLocation);
    }
}

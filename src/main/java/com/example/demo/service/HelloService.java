package com.example.demo.service;

import com.example.demo.model.RequestMessage;
import com.example.demo.model.ResponseMessage;
import org.springframework.stereotype.Service;

@Service
public class HelloService {
    public ResponseMessage hello(RequestMessage requestMessage) {
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setResult(String.format("Hello, %s!", requestMessage.getName()));
        return responseMessage;
    }
}

package com.example.demo.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "Объект ответа")
public class ResponseMessage {
    @ApiModelProperty(notes = "Строка с результатом", example = "Hello, Marina!")
    private String result;
}

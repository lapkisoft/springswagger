package com.example.demo.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "Объект запроса")
public class RequestMessage {
    @ApiModelProperty(notes = "Просто любое имя", example = "Marina")
    private String name;
}
